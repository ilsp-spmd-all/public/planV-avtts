
SCRIPT_PATH="$(dirname $(readlink -f $0))"

#USER_DIR=/taco/tts_service
USER_DIR=$(pwd)

SERVICE_HOST=${1:-0.0.0.0}
SERVICE_PORT=${2:-5000}
CONFIG_FILE="workspace/avatar/service_audio/config.json"

echo "Running on http://$SERVICE_HOST:$SERVICE_PORT"
echo "script path is ${SCRIPT_PATH}"


PYTHONPATH=${PYTHONPATH}:/workspace/avatar 
xvfb-run python $SCRIPT_PATH/predict.py \
    --webhost $SERVICE_HOST \
    --webport $SERVICE_PORT \
    --configs $CONFIG_FILE \
    --user_dir $USER_DIR
