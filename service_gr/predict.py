import argparse
import json
import os
import uuid
import unicodedata
from functools import lru_cache

import base64
from io import BytesIO, StringIO
from flask import (Flask, jsonify, make_response, render_template, request,
                   send_file, url_for)
from scipy.io import wavfile
from scipy.signal import resample

print(f"aLL FILES INSIDE CONTAINER")
print(os.listdir("/workspace/avatar/"))
print(f"The cwd is {os.getcwd()}")

from flask_cors import CORS, cross_origin
from tts_service.infer import make_tts_fn
from tts_service.infer import parse_args as parse_tts_args

from av_synthesis import make_kripis_fn, parse_args_kripis

# from examples.spell_corrector.predictor import make_corrector_for_website


def get_uuid():
    id = uuid.uuid1()
    return str(id.hex)


def parser_for_website():
    parser = argparse.ArgumentParser()
    parser = parse_tts_args(parser)
    parser = parse_args_kripis(parser)
    parser.add_argument("--webhost", type=str, default="0.0.0.0", help="Hostname")
    parser.add_argument("--webport", type=int, default=5000, help="Port")
    parser.add_argument(
        "--configs",
        type=str,
        default="/workspace/avatar/tts_service/config.json",
        help="the configuration file",
    )
    parser.add_argument(
        "--user_dir", type=str, default="/workspace/avatar/", help="User directory"
    )

    args = parser.parse_args()

    return args, parser


ARGS, PARSER = parser_for_website()

SERVICE_HOST, PORT, CONFIG, USER_DIR = (
    ARGS.webhost,
    ARGS.webport,
    ARGS.configs,
    ARGS.user_dir,
)

print(f"aLL FILES INSIDE CONTAINER")
print(os.listdir("/workspace/avatar/"))

with open("/workspace/avatar/service_gr/config.json", "r") as fd:
    args = json.load(fd)

# overwrite arguments
ARGS.jpg = args["jpg"]
ARGS.jpg_bg = args["jpg_bg"]
ARGS.dump_folder = args["dump_folder"]
ARGS.load_AUTOVC_name = args["autovc"]
ARGS.load_a2l_G_name = args["spk_branch"]
ARGS.load_a2l_C_name = args["content_branch"]
ARGS.load_G_name = args["i2i"]
ARGS.output_folder = args["output_folder"]
ARGS.input_folder = args["input_folder"]

generate_wav = make_tts_fn(
    PARSER,
    args["tacotron2"],
    args["waveglow"],
    args["text_cleaners"],
    args["sigma_infer"],
    args["denoising_strength"],
    args["stft_hop_length"],
    args["cpu"],
    args["fp16"],
    args["sr"],
    USER_DIR,
    args["speaker_id"],
    16000,
)

generate_kripis = make_kripis_fn(
    ARGS,
    "/workspace/avatar/ckpt_116_i2i_comb.pth",
    "/workspace/avatar/ckpt_autovc.pth",
    "/workspace/avatar/ckpt_speaker_branch.pth",
    "/workspace/avatar/ckpt_content_branch.pth",
)

print("SERVICE IS READY!")

app = Flask(__name__)
cors = CORS(app)
app.config["CORS_HEADERS"] = "Content-Type"


@app.route("/", methods=["GET"])
def index():
    return render_template(
        "index.html", service_url="http://{}:{}/kripis".format(SERVICE_HOST, PORT)
    )


@app.route("/tts", methods=["POST"])
def tts():
    sentence = request.json["sentence"]
    print(f"The input sentece is {sentence}")
    wav = generate_wav(sentence, save=False, b64=False)
    response = make_response(wav)
    response.headers["Content-Type"] = "audio/wav"
    response.headers["Content-Disposition"] = "attachment; filename=sound.wav"
    response.status = 200

    return response


@app.route("/kripis", methods=["POST"])
def kripis():
    sentence = request.json["sentence"]
    req_id= get_uuid()
    out_dir = os.path.join("/workspace/avatar/av_requests/", req_id)
    os.mkdir(out_dir)
    print(f"Succesfully created the output directory in {out_dir}")
    wav = generate_wav(sentence, req_id, save=True, b64=False)
    # wav_np = wav.squeeze(0).detach().cpu().numpy()
    # downsample
    # wavfile.write(os.path.join(out_dir, "out.wav"), 16000, wav)
    # wav = wavfile.read(os.path.join(out_dir, "out.wav"))
    # wav_ds = resample(wav_np, int(len(wav_np) * 16000 / 22050))
    # wavfile.write(os.path.join(out_dir, "out.wav"), 16000, wav)
    # wavfile.write("/workspace/avatar/out.wav", 16000, wav)
    # with open(os.path.join(out_dir, "out.wav"), "wb") as fd:
    #     fd.write(wav)
    # print(f"Has writen generated wav in {out_dir} as 'out.wav'")
    mp4 = generate_kripis("out.wav", save_id=req_id)
    with open(mp4, "rb") as fd:
        mp4_data = fd.read()

    response = make_response(mp4_data)
    response.headers["Content-Type"] = "video/mp4"
    response.headers["Content-Disposition"] = "attachment; filename=kripis.mp4"
    response.status = 200

    return response


@app.route("/kripisb64", methods=["POST"])
def kripisb64():
    sentence = request.json["sentence"]
    req_id= get_uuid()
    out_dir = os.path.join("/workspace/avatar/av_requests/", req_id)
    os.mkdir(out_dir)
    print(f"Succesfully created the output directory in {out_dir}")
    wav = generate_wav(sentence, req_id, save=True, b64=False)
    # wav_np = wav.squeeze(0).detach().cpu().numpy()
    # wavfile.write(os.path.join(out_dir, "out.wav"), 16000, wav)
    # wav_ds = resample(wav_np, int(len(wav_np) * 16000 / 22050))

    # write buffered audio
    # with open(os.path.join(out_dir, "out.wav"), "wb") as fd:
    #     fd.write(wav)
    # wavfile.write(os.path.join(out_dir, "out.wav"), 16000, wav)
    # wavfile.write("/workspace/avatar/out.wav", 16000, wav)
    print(f"Has writen generated wav in {out_dir} as 'out.wav'")
    mp4 = generate_kripis("out.wav", save_id=req_id)
    with open(mp4, "rb") as fd:
        mp4_data = fd.read()

    with BytesIO() as buf:
        buf.write(mp4_data)
        b64vid = base64.b64encode(buf.getvalue()).decode("UTF-8")
    response = app.response_class(
        response=json.dumps(b64vid), status=200, mimetype="application/json"
    )

    return response

@app.route("/ttsb64", methods=["POST"])
def tts64():
    sentence = request.json["sentence"]
    print(f"The input sentece is {sentence}")
    b64wav = generate_wav(sentence, save=False, b64=True)
    response = app.response_class(
        response=json.dumps(b64wav), status=200, mimetype="application/json"
    )

    return response
    # sentence = unicodedata.normalize("NFD", sentence)
    # path_to_wav = generate_wav(sentence)

    # print(f"The exported path to the wav file is {path_to_wav}")
    # return send_file(path_to_wav,
    #                 mimetype="audio/wav",
    #                 as_attachment=True,
    #                 attachment_filename="test.wav",
    #                 cache_timeout=0)
    # corrected = chunk_spell_correct(sentence)  # spell_correct([sentence])

    # return jsonify({"wav": wav})


if __name__ == "__main__":
    from waitress import serve

    serve(app, host="0.0.0.0", port=PORT)
