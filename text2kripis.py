import os
import argparse
import shutil
import pickle
import glob
import numpy as np
from av_synthesis import parse_args_kripis, make_kripis_fn
from tts_service.infer import make_tts_fn
from tts_service.infer import parse_args as parse_args_taco


if __name__ == "__main__":
    """
    Launches text to speech (inference).
    Inference is executed on a single GPU or CPU.
    """
    parser = argparse.ArgumentParser(description="PyTorch Tacotron 2 Inference")
    parser = parse_args_taco(parser)
    args, _ = parser.parse_known_args()
    args.input = "av_requests/text"
    input_path = args.input
    text_cleaners = args.text_cleaners
    args.cpu = True

    generate_wav = make_tts_fn(
        parser,
        args.tacotron2,
        args.waveglow,
        args.text_cleaners,
        args.sigma_infer,
        args.denoising_strength,
        args.stft_hop_length,
        args.cpu,
        args.fp16,
    )

    with open(os.path.join(input_path, "e2e.txt")) as fd:
        sentence = fd.readline()

    audio_path = generate_wav(sentence, save=True)
    print(f"Generated audio saved at {audio_path}")

    parser = argparse.ArgumentParser(description="Kripis Avatar Generation")
    parser = parse_args_kripis(parser)
    kripis_parser = parser.parse_args()

    # path to checkpoints
    i2i = "/workspace/avatar/ckpt_116_i2i_comb.pth"
    autovc = "/workspace/avatar/ckpt_autovc.pth"
    spk_branch = "/workspace/avatar/ckpt_speaker_branch.pth"
    content_branch = "/workspace/avatar/ckpt_content_branch.pth"

    # path to IO folders
    input_folder = "/workspace/avatar/av_requests/wav/"
    output_folder = "/workspace/avatar/av_requests/mp4/"
    # dump folder
    dump_folder = "/workspace/avatar/av_speech_synthesis/tmp/dump"
    # cpu arg
    cpu = True

    # overwrite args
    kripis_parser.dump_folder = dump_folder
    kripis_parser.load_AUTOVC_name = autovc
    kripis_parser.load_a2l_G_name = spk_branch
    kripis_parser.load_a2l_C_name = content_branch
    kripis_parser.load_G_name = i2i
    kripis_parser.output_folder = output_folder

    # get main wav--->mp4 script
    generate_kripis = make_kripis_fn(
        kripis_parser,
        i2i, autovc, spk_branch, content_branch,
        amp_lip_x=2.0,
        amp_lip_y=2.0,
        amp_pos=0.5,
        output_folder=output_folder,
        input_folder=input_folder,
        dump_folder=dump_folder,
        cpu=cpu
    )

    # just the name of the wav since the i/o folders are already given
    generate_kripis("e2e.wav")
