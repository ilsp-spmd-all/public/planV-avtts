#!/bin/bash

docker run --shm-size=1g --ulimit memlock=-1 --ulimit stack=67108864 -it --rm --ipc=host -v $PWD:/workspace/tacotron2/ -v /data/logotypografia_simple/preproc_deepmind_repo/wavs_trim_DM:/workspace/tacotron2/greek/wavs -v /data/logotypografia_simple/preproc_deepmind_repo/melspec_trim_DM:/workspace/tacotron2/greek/mels -v /data/tacotron2/en/waveglow_single_output:/workspace/tacotron2/pretrained_waveglow tacotron2 bash
