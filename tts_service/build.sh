docker build -f Dockerfile -t tts_example_service/en:0.1 \
    --build-arg SERVICE_HOST=192.168.188.156 \
    --build-arg SERVICE_PORT=6066 \
    --build-arg TACO_MODEL=tacotron2.pt \
    --build-arg GLOW_MODEL=waveglow.pt \
    . 
