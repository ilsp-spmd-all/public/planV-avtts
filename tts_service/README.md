# TTS Service

## Prerequisites:
- Acces to AthenaRC VPN

- Acces to milos for our checkpoints. Otherwise download nvidia's/pytorch's pretrained models 

## QA Steps:

- goto `/home/egeorgiou/avtts/tts-service`

- either copy checkpoints from that folder to your local clone of the repo or go to my path instead

## Service Setup Steps:

- **Tacotron2**: store either our own pretrained model from '/data/tacotron2/en/taco_single_output/checkpoint_Tacotron2_1500.pt' or the own you downloaded from PyTorch's zoo under the folder tts_service as `tacotron2.pt`

- **WaveGlow**: The same as above. THe vocoder we also use is the pretrained from nvidia which is at '/data/tacotron2/trained_models/waveglow/nvidia_waveglowpyt_fp32_20190306.pth' as `waveglow.pt`
- `bash build.sh`: builds Docker image
- `docker run -p 6066:6066 --name <your_container_name> -it tts_example_service/en:0.1`
- after experimenting/code reviewing
- `docker stop <your_container_name>`
- `docker rm <your_container_name>`

## Additional Info/ TODOs:
- you can change the deployment port in the `build.sh` script
