import argparse
import base64
import os
import sys
import time
import librosa
from io import BytesIO, StringIO
import subprocess

import torch
from posixpath import join
from scipy.io.wavfile import write, read
from scipy.signal import resample


import tts_service.models as models
from tts_service.tacotron2.text import text_to_sequence
from tts_service.waveglow.denoiser import Denoiser


def check_directory_and_create(dir_path, exists_warning=False):
    """
    Checks if the path specified is a directory or creates it if it doesn't
    exist.
    Args:
        dir_path (string): directory path to check/create
    Returns:
        (string): the input path
    """

    if os.path.exists(dir_path):
        if not os.path.isdir(dir_path):
            raise ValueError(f"Given path {dir_path} is not a directory")
        elif exists_warning:
            print(
                f"WARNING: Already existing experiment folder {dir_path}."
                "It is recommended to change experiment_id in "
                "configs/exp_config.json file. Proceeding by overwriting"
            )
    else:
        os.mkdir(dir_path)

    return os.path.abspath(dir_path)


def parse_args(parser):
    """
    Parse commandline arguments.
    """
    parser.add_argument(
        "-i",
        "--input",
        type=str,
        required=False,
        help="full path to the input text (phrases separated by new line)",
    )
    parser.add_argument(
        "-o",
        "--output",
        required=False,
        default="requests/wavs",
        help="output folder to save audio (file per phrase)",
    )
    parser.add_argument(
        "--suffix", type=str, default="web", help="output filename suffix"
    )
    parser.add_argument(
        "--custom_name",
        default=False,
        action="store_true",
        help="When used the generated .wav's are named as id.wav",
    )
    parser.add_argument(
        "--tacotron2",
        type=str,
        default="tacotron2.pt",
        help="full path to the Tacotron2 model checkpoint file",
    )
    parser.add_argument(
        "--waveglow",
        type=str,
        default="waveglow.pt",
        help="full path to the WaveGlow model checkpoint file",
    )
    parser.add_argument("-s", "--sigma-infer", default=0.9, type=float)
    parser.add_argument("-d", "--denoising-strength", default=0.01, type=float)
    parser.add_argument(
        "-sr", "--sampling-rate", default=22050, type=int, help="Sampling rate"
    )
    parser.add_argument(
        "-preproc",
        "--text-cleaners",
        nargs="*",
        default=["english_cleaners"],
        type=str,
        help="Type of text cleaners for input text",
    )
    parser.add_argument('--speaker_id',
                        type=int, default=None,
                        help='speaker id')
    # audio parameters
    audio = parser.add_argument_group("audio parameters")
    audio.add_argument(
        "--max-wav-value", default=32768.0, type=float, help="Maximum audiowave value"
    )
    audio.add_argument("--filter-length", default=1024, type=int, help="Filter length")
    audio.add_argument(
        "--hop-length", default=256, type=int, help="Hop (stride) length"
    )
    audio.add_argument("--win-length", default=1024, type=int, help="Window length")
    audio.add_argument(
        "--mel-fmin", default=0.0, type=float, help="Minimum mel frequency"
    )
    audio.add_argument(
        "--mel-fmax", default=8000.0, type=float, help="Maximum mel frequency"
    )

    run_mode = parser.add_mutually_exclusive_group()
    run_mode.add_argument(
        "--fp16", action="store_true", help="Run inference with mixed precision"
    )
    run_mode.add_argument("--cpu", action="store_true", help="Run inference on CPU")

    parser.add_argument(
        "--log-file", type=str, default="nvlog.json", help="Filename for logging"
    )
    parser.add_argument("--include-warmup", action="store_true", help="Include warmup")
    parser.add_argument(
        "--stft-hop-length",
        type=int,
        default=256,
        help="STFT hop length for estimating audio length from mel size",
    )

    return parser


def checkpoint_from_distributed(state_dict):
    """
    Checks whether checkpoint was generated by DistributedDataParallel. DDP
    wraps model in additional "module.", it needs to be unwrapped for single
    GPU inference.
    :param state_dict: model's state dict
    """
    ret = False

    for key, _ in state_dict.items():
        if key.find("module.") != -1:
            ret = True

            break

    return ret


def unwrap_distributed(state_dict):
    """
    Unwraps model from DistributedDataParallel.
    DDP wraps model in additional "module.", it needs to be removed for single
    GPU inference.
    :param state_dict: model's state dict
    """
    new_state_dict = {}

    for key, value in state_dict.items():
        new_key = key.replace("module.", "")
        new_state_dict[new_key] = value

    return new_state_dict


def load_and_setup_model(
    model_name, parser, checkpoint, fp16_run, cpu_run, forward_is_infer=False
):
    print(f"--------Model Name is {model_name}")
    model_parser = models.parse_model_args(model_name, parser, add_help=False)
    model_args, _ = model_parser.parse_known_args()
    model_config = models.get_model_config(model_name, model_args)
    model = models.get_model(
        model_name, model_config, cpu_run=cpu_run, forward_is_infer=forward_is_infer
    )

    if checkpoint is not None:
        if cpu_run:
            state_dict = torch.load(checkpoint, map_location=torch.device("cpu"))[
                "state_dict"
            ]
        else:
            state_dict = torch.load(checkpoint)["state_dict"]

        if checkpoint_from_distributed(state_dict):
            state_dict = unwrap_distributed(state_dict)

        model.load_state_dict(state_dict)

    if model_name == "WaveGlow":
        model = model.remove_weightnorm(model)

    model.eval()

    if fp16_run:
        model.half()

    return model


# taken from tacotron2/data_function.py:TextMelCollate.__call__
def pad_sequences(batch):
    # Right zero-pad all one-hot text sequences to max input length
    input_lengths, ids_sorted_decreasing = torch.sort(
        torch.LongTensor([len(x) for x in batch]), dim=0, descending=True
    )
    max_input_len = input_lengths[0]

    text_padded = torch.LongTensor(len(batch), max_input_len)
    text_padded.zero_()

    for i in range(len(ids_sorted_decreasing)):
        text = batch[ids_sorted_decreasing[i]]
        text_padded[i, : text.size(0)] = text

    return text_padded, input_lengths


def prepare_input_sequence(texts, cpu_run=False, text_cleaners=["english_cleaners"], greek=False):

    d = []

    for i, text in enumerate(texts):
        d.append(torch.IntTensor(text_to_sequence(text, text_cleaners, greek=greek)[:]))

    text_padded, input_lengths = pad_sequences(d)

    if not cpu_run:
        text_padded = text_padded.cuda().long()
        input_lengths = input_lengths.cuda().long()
    else:
        text_padded = text_padded.long()
        input_lengths = input_lengths.long()

    return text_padded, input_lengths


class MeasureTime:
    def __init__(self, measurements, key, cpu_run=False):
        self.measurements = measurements
        self.key = key
        self.cpu_run = cpu_run

    def __enter__(self):
        if not self.cpu_run:
            torch.cuda.synchronize()
        self.t0 = time.perf_counter()

    def __exit__(self, exc_type, exc_value, exc_traceback):
        if not self.cpu_run:
            torch.cuda.synchronize()
        self.measurements[self.key] = time.perf_counter() - self.t0


def make_tts_fn(
    parser,
    tacotron2,
    waveglow,
    text_cleaners=["english_cleaners"],
    sigma_infer=0.9,
    denoising_strength=0.01,
    stft_hop_length=256,
    cpu=True,
    fp16=False,
    sr=22050,
    user_path="/workspace/avatar",
    speaker_id=None,
    avatar_sr=16000,
):
    if speaker_id is not None:
        print(f"Loading Regotron ---------------------------")
        # essentially used to handle the multispeaker case
        tacotron2 = load_and_setup_model(
            "Regotron", parser, tacotron2, fp16, cpu, forward_is_infer=True
        )
        greek=True
    else:
        print(f"Loading Tacotron2 ---------------------------")
        tacotron2 = load_and_setup_model(
            "Tacotron2", parser, tacotron2, fp16, cpu, forward_is_infer=True
        )
        greek=False

    waveglow = load_and_setup_model(
        "WaveGlow", parser, waveglow, fp16, cpu, forward_is_infer=True
    )
    denoiser = Denoiser(waveglow)
    jitted_tacotron2 = torch.jit.script(tacotron2)

    def generate_wav(sentence, uid, save=False, b64=False):
        print(f"~~~~~~~ The input sequence is {sentence}")
        texts = [sentence]
        sequences_padded, input_lengths = prepare_input_sequence(
            texts, cpu, text_cleaners, greek
        )
        print(f"Padded Sequences ---> {sequences_padded}")
        print(speaker_id)
        if speaker_id is not None:
            spk_ids = torch.IntTensor([speaker_id-1]).long()

        measurements = {}
        with torch.no_grad(), MeasureTime(measurements, "tacotron2_time", cpu):
            if speaker_id is not None:
                mel, mel_lengths, _ = \
                    jitted_tacotron2(sequences_padded, input_lengths, spk_ids)
            else:
                mel, mel_lengths, _ = \
                    jitted_tacotron2(sequences_padded, input_lengths)

        with torch.no_grad(), MeasureTime(measurements, "waveglow_time", cpu):
            audios = waveglow(mel, sigma=sigma_infer)
            audios = audios.float()
        with torch.no_grad(), MeasureTime(measurements, "denoiser_time", cpu):
            audios = denoiser(audios, strength=denoising_strength).squeeze(1)

        print(f"Audios shape is {audios.size()}")

        for i, audio in enumerate(audios):
            audio = audio[: mel_lengths[i] * stft_hop_length]
            audio = audio / torch.max(torch.abs(audio))

            break

        audio = audio.cpu().numpy()

        if save:
            path_to_audio = os.path.join(user_path, "av_requests", uid, "out_unpad.wav")
            print(
                f"The path to the audio file which is about to be saved is {path_to_audio}"
            )
            audio_ds = librosa.resample(audio, orig_sr=sr, target_sr=avatar_sr)
            write(path_to_audio, avatar_sr, audio_ds)
            path_to_pad_audio = os.path.join(user_path, "av_requests", uid, "out.wav")
            command = ["sox", path_to_audio, path_to_pad_audio, "pad", "0", "1"]
            subprocess.run(command)
            _, audio_ds_pad = read(path_to_pad_audio)
            return audio_ds_pad
        else:
            # resample audio to avatar_sr
            audio_ds = librosa.resample(audio, orig_sr=sr, target_sr=avatar_sr)
            path_to_audio = os.path.join(user_path, "av_requests", uid, "out_unpad.wav")
            write(path_to_audio, avatar_sr, audio_ds)
            path_to_pad_audio = os.path.join(user_path, "av_requests", uid, "out.wav")
            command = ["sox", path_to_audio, path_to_pad_audio, "pad", "0", "1"]
            subprocess.run(command)
            _, audio_ds_pad = read(path_to_pad_audio)
            # audio_ds = resample(audio, int(len(audio) * avatar_sr / sr))
            with BytesIO() as buf:
                write(buf, avatar_sr, audio_ds_pad)
                if b64:
                    audio_ds_pad = base64.b64encode(buf.getvalue()).decode("UTF-8")
                else:
                    audio_ds_pad = buf.getvalue()
            # audio = base64.b64encode(audio).decode("UTF-8")

            return audio_ds_pad

    return generate_wav


def main():
    """
    Launches text to speech (inference).
    Inference is executed on a single GPU or CPU.
    """
    parser = argparse.ArgumentParser(description="PyTorch Tacotron 2 Inference")
    parser = parse_args(parser)
    args, _ = parser.parse_known_args()
    input_path = args.input
    text_cleaners = args.text_cleaners

    # check_directory_and_create(args.output, exists_warning=True)

    # tacotron2 = load_and_setup_model('Tacotron2', parser, args.tacotron2,
    #                                  args.fp16, args.cpu,
    #                                  forward_is_infer=True)
    # waveglow = \
    #     load_and_setup_model('WaveGlow', parser, args.waveglow,
    #                          args.fp16, args.cpu, forward_is_infer=True)
    # denoiser = Denoiser(waveglow)
    # if not args.cpu:
    #     denoiser.cuda()

    # jitted_tacotron2 = torch.jit.script(tacotron2)

    texts = []
    try:
        f = open(args.input, "r")
        texts = f.readlines()
    except:
        print("Could not read file")
        sys.exit(1)

    # one sentence scenario
    sentence = texts[0]

    generate_wav = make_tts_fn(
        parser,
        args.tacotron2,
        args.waveglow,
        args.text_cleaners,
        args.sigma_infer,
        args.denoising_strength,
        args.stft_hop_length,
        args.cpu,
        args.fp16,
    )

    audio_path = generate_wav(sentence)

    print(f"Generated audio saved at {audio_path}")
    # audio_path = \
    #     os.path.join(args.output, "audio_"+args.suffix+".wav")
    # write(audio_path, args.sampling_rate, audio)


if __name__ == "__main__":
    main()
