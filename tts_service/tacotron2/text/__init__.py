""" from https://github.com/keithito/tacotron """
import re
# from nvidia_tacotron2.tacotron2.text import cleaners
# from nvidia_tacotron2.tacotron2.text.symbols import symbols, greek_symbols, utf8_symbols
from tacotron2.text import cleaners
from tacotron2.text.symbols import symbols, greek_symbols


# Mappings from symbol to numeric ID and vice versa:
_symbol_to_id = {s: i for i, s in enumerate(symbols)}
_id_to_symbol = {i: s for i, s in enumerate(symbols)}

_symbol_to_id_greek = {s: i for i, s in enumerate(greek_symbols)}
_id_to_symbol_greek = {i: s for i, s in enumerate(greek_symbols)}

# Regular expression matching text enclosed in curly braces:
_curly_re = re.compile(r'(.*?)\{(.+?)\}(.*)')


def text_to_sequence(text, cleaner_names, greek=False):
  '''Converts a string of text to a sequence of IDs corresponding to the symbols in the text.

    The text can optionally have ARPAbet sequences enclosed in curly braces embedded
    in it. For example, "Turn left on {HH AW1 S S T AH0 N} Street."

    Args:
      text: string to convert to a sequence
      cleaner_names: names of the cleaner functions to run the text through

    Returns:
      List of integers corresponding to the symbols in the text
  '''
  sequence = []
  # print(f"Initial tex is {text}")
  # Check for curly braces and treat their contents as ARPAbet:
  while len(text):
    # import pdb; pdb.set_trace()
    m = _curly_re.match(text)
    if not m:
      sequence += _symbols_to_sequence(
        _clean_text(text, cleaner_names), greek=greek
      )
      break
    sequence += _symbols_to_sequence(_clean_text(m.group(1), cleaner_names), greek=greek)
    sequence += _arpabet_to_sequence(m.group(2), greek=greek)
    # print(f"sequence is {sequence}")
    text = m.group(3)
  # import pdb; pdb.set_trace()
  ### for debugging
  # print(text)
  # print(_id_to_symbol_greek)
  # print(_symbol_to_id_greek)
  # print(sequence)
  # reconstruct = []
  # for s in sequence:
  #   reconstruct.append(_id_to_symbol_greek[s])
  # print(f"Reconstructed seq is {reconstruct}")
  # import pdb; pdb.set_trace()
  return sequence


def sequence_to_text(sequence):
  '''Converts a sequence of IDs back to a string'''
  result = ''
  for symbol_id in sequence:
    if symbol_id in _id_to_symbol:
      s = _id_to_symbol[symbol_id]
      # Enclose ARPAbet back in curly braces:
      if len(s) > 1 and s[0] == '@':
        s = '{%s}' % s[1:]
      result += s
  return result.replace('}{', ' ')


def _clean_text(text, cleaner_names):
  for name in cleaner_names:
    cleaner = getattr(cleaners, name)
    if not cleaner:
      raise Exception('Unknown cleaner: %s' % name)
    text = cleaner(text)
  return text


def _symbols_to_sequence(symbols, greek=False):#, utf8=False):
  # if utf8:
  #   return [_symbol_to_id_utf8[s] for s in symbols]
  # else:
  if greek:
    return [_symbol_to_id_greek[s] for s in symbols if _should_keep_symbol_greek(s)]
  else:
    return [_symbol_to_id[s] for s in symbols if _should_keep_symbol(s)]

def _arpabet_to_sequence(text, greek=False):
  return _symbols_to_sequence(['@' + s for s in text.split()], greek=greek)

def _should_keep_symbol_greek(s):
  return s in _symbol_to_id_greek and s != '_' and s != '~'

def _should_keep_symbol(s):
  return s in _symbol_to_id and s != '_' and s != '~'
