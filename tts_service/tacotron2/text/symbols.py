""" from https://github.com/keithito/tacotron """

'''
Defines the set of symbols used in text input to the model.

The default is a set of ASCII characters that works well for English or text that has been run through Unidecode. For other data, you can modify _characters. See TRAINING_DATA.md for details. '''
# from nvidia_tacotron2.tacotron2.text import cmudict
from tacotron2.text import cmudict


_pad        = '_'
_punctuation = '!\'(),.:;? '
_special = '-'
_letters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'

_punctuation_plus = ' ’/…'

_greek_letters = 'ΑΒΓΔΕΖΗΘΙΚΛΜΝΞΟΠΡΣΤΥΦΨΩΧαβγδεζηθικλμνξοπρςστυφχψωΐάέήίΰϊϋόύώ'
_greek_letters_init_mapping = 'ABKDEZETIKLMNKOPRSTIFPOXabkdezetiklmnkoprsstifxpoiaeiiiiioio'

# _other = '/`·…'
# _other = '/`·…'

# Prepend "@" to ARPAbet symbols to ensure uniqueness (some are the same as uppercase letters):
_arpabet = ['@' + s for s in cmudict.valid_symbols]

# Export all symbols:
symbols = [_pad] + list(_special) + list(_punctuation) + list(_letters) \
          + _arpabet

greek_symbols = [_pad] + list(_special) + list(_punctuation) + list(_letters) \
          + _arpabet + list(_punctuation_plus) + list(_greek_letters)

init_greek_mapping = {}
for k,v in zip(_greek_letters, _greek_letters_init_mapping):
  init_greek_mapping[k] = v

utf8_symbols = []
for k in symbols:
    utf8_symbols.append(k.encode('utf-8'))
