""" from https://github.com/keithito/tacotron """

'''
Cleaners are transformations that run over the input text at both training and eval time.

Cleaners can be selected by passing a comma-delimited list of cleaner names as the "cleaners"
hyperparameter. Some cleaners are English-specific. You'll typically want to use:
  1. "english_cleaners" for English text
  2. "transliteration_cleaners" for non-English text that can be transliterated to ASCII using
     the Unidecode library (https://pypi.python.org/pypi/Unidecode)
  3. "basic_cleaners" if you do not want to transliterate (in this case, you should also update
     the symbols in symbols.py to match your data).
'''
import os
import re
import copy
from unidecode import unidecode
import unicodedata
from .numbers import normalize_numbers


# _logotypo_re = re.compile(r"\[[^[]]*\)+")
_logotypo_re = re.compile(r"\[[^][]*\]")
_asterisk_re = re.compile(r"\*")
_left_parenthesis = re.compile(r"\-\(")
_right_parenthesis = re.compile(r"\)\-")

# Regular expression matching whitespace:
_whitespace_re = re.compile(r'\s+')

# List of (regular expression, replacement) pairs for abbreviations:
_abbreviations = [(re.compile('\\b%s\\.' % x[0], re.IGNORECASE), x[1]) for x in [
  ('mrs', 'misess'),
  ('mr', 'mister'),
  ('dr', 'doctor'),
  ('st', 'saint'),
  ('co', 'company'),
  ('jr', 'junior'),
  ('maj', 'major'),
  ('gen', 'general'),
  ('drs', 'doctors'),
  ('rev', 'reverend'),
  ('lt', 'lieutenant'),
  ('hon', 'honorable'),
  ('sgt', 'sergeant'),
  ('capt', 'captain'),
  ('esq', 'esquire'),
  ('ltd', 'limited'),
  ('col', 'colonel'),
  ('ft', 'fort'),
]]

def lowercase(text):
    return text.lower()

def collapse_whitespace(text):
    return re.sub(_whitespace_re, ' ', text)


def collapse_logotypografia(text):
    # return re.sub(r"\[.*\]", "", text)
    return re.sub(_logotypo_re, "", text)


def remove_tonous(text):
    return unicodedata.normalize("NFD", text)

def get_tonous(text):
    return unicodedata.normalize("NFKD", text)

def remove_stars(text):
    return re.sub(_asterisk_re, "", text)

def remove_double_brackets(text):
  text = text.replace("»", "")
  text = text.replace("“", "")
  text = text.replace("”", "")
  text = text.replace("_", " ")
  text = text.replace("«", "")
  text = text.replace("-","-")
  text = text.replace(" "," ")
  text = text.replace("-", "-")
  return text
  

def remove_L_bracket(text):
    return re.sub("<", "", text)


def remove_R_bracket(text):
    return re.sub(">", "", text)


def remove_R_par(text):
    return re.sub("\)", "", text)


def remove_L_par(text):
    return re.sub("\(", "", text)


def remove_right_par_dash(text):
    return re.sub(_right_parenthesis, "", text)


def remove_left_dash_par(text):
    return re.sub(_left_parenthesis, "", text)


def convert_to_ascii(text):
    return unidecode(text)


def collapse_tilde(text):
    return re.sub("~", "", text)

def clean_parenthesis(text):
    s = copy.copy(text)
    s = remove_left_dash_par(s)
    s = remove_right_par_dash(s)
    s = remove_L_par(s)
    s = remove_R_par(s)
    return s


def clean_brackets(text):
    text = remove_L_bracket(text)
    text = remove_R_bracket(text)
    return text

# def remove_non_ascii(string):
#     return ''.join(char for char in string if ord(char) < 128)

# print(remove_non_ascii('a€bñcá'))  # 👉️ 'abc'
# print(remove_non_ascii('a_b^0'))  # 👉️ a_b^0


def _clean_text(text):
    """Custom pipeline for cleaning greek text. The pipeline consists of
    removing whitespaces, asterisks, tildes, brackets, parenthesis and
    the special logotypografia [noise] strings.
    """
    s = copy.copy(text)
    # clean text
    s = collapse_whitespace(s)
    s = collapse_tab(s)
    s = remove_stars(s)
    s = collapse_tilde(s)
    s = clean_brackets(s)
    s = clean_parenthesis(s)
    s = collapse_logotypografia(s)
    s = remove_double_brackets(s)
    s = s.replace(".", ".")
    s = s.replace(".", ".")
    s = s.replace("–", "-")
    s = s.replace("[", "")
    s = s.replace("]", "")
    s = s.replace("{", "")
    s = s.replace("}", "")
    s = s.replace("­", "")
    s = s.replace("·", ",")
    s = s.replace("`", "")
    s = s.replace("·", "")
    s = s.lstrip()
    s = collapse_whitespace(s)
    # convert to ascii (the following lines may vary according to the
    # predifined preprocessing.)
    # s = lowercase(s)
    # s = get_tonous(s)
    # s = convert_to_ascii(s)
    return s

def expand_abbreviations(text):
  for regex, replacement in _abbreviations:
    text = re.sub(regex, replacement, text)
  return text


def expand_numbers(text):
  return normalize_numbers(text)


def lowercase(text):
  return text.lower()


def collapse_whitespace(text):
  return re.sub(_whitespace_re, ' ', text)

def collapse_tab(text):
  return text.replace("\t", " ")

def convert_to_ascii(text):
  return unidecode(text)


def basic_cleaners(text):
  '''Basic pipeline that lowercases and collapses whitespace without transliteration.'''
  text = lowercase(text)
  text = collapse_whitespace(text)
  return text


def transliteration_cleaners(text):
  '''Pipeline for non-English text that transliterates to ASCII.'''
  text = convert_to_ascii(text)
  text = lowercase(text)
  text = collapse_whitespace(text)
  return text


def english_cleaners(text):
  '''Pipeline for English text, including number and abbreviation expansion.'''
  text = convert_to_ascii(text)
  text = lowercase(text)
  text = expand_numbers(text)
  text = expand_abbreviations(text)
  text = collapse_whitespace(text)
  return text

def greek_cleaners(text):
  text =  _clean_text(text)
  text = lowercase(text)
  text = collapse_whitespace(text)
  return text

def find_unique_chars(data_samples, verbose=True):
    texts = "".join(item for item in data_samples)
    chars = set(texts)
    lower_chars = filter(lambda c: c.islower(), chars)
    chars_force_lower = [c.lower() for c in chars]
    chars_force_lower = set(chars_force_lower)

    # for k in chars:
    #   print(f"[start]{k}[end]")
    if verbose:
        print(f" > Number of unique characters: {len(chars)}")
        print(f" > Unique characters: {''.join(sorted(chars))}")
        print(f" > Unique lower characters: {''.join(sorted(lower_chars))}")
        print(f" > Unique all forced to lower characters: {''.join(sorted(chars_force_lower))}")
    return chars_force_lower
