import argparse
import json
import os
import unicodedata
from functools import lru_cache

from flask import (Flask, jsonify, make_response, render_template, request,
                   send_file, url_for)

from flask_cors import CORS, cross_origin
from infer import make_tts_fn, parse_args

# from examples.spell_corrector.predictor import make_corrector_for_website


def parser_for_website():
    parser = argparse.ArgumentParser()
    parser.add_argument("--webhost", type=str, default="0.0.0.0", help="Hostname")
    parser.add_argument("--webport", type=int, default=5000, help="Port")
    parser.add_argument(
        "--configs", type=str, default="config.json", help="the configuration file"
    )
    parser.add_argument(
        "--user_dir", type=str, default="taco/tts_service", help="User directory"
    )

    args = parser.parse_args()

    return args.webhost, args.webport, args.configs, args.user_dir


SERVICE_HOST, PORT, CONFIG, USER_DIR = parser_for_website()

with open(os.path.join(USER_DIR, CONFIG)) as fd:
    args = json.load(fd)

parser = argparse.ArgumentParser(description="TACOTRON2 TTS SERVICE")
parser = parse_args(parser)

generate_wav = make_tts_fn(
    parser,
    args["tacotron2"],
    args["waveglow"],
    args["text_cleaners"],
    args["sigma_infer"],
    args["denoising_strength"],
    args["stft_hop_length"],
    args["cpu"],
    args["fp16"],
    args["sr"],
    user_path=USER_DIR,
)

app = Flask(__name__)
cors = CORS(app)
app.config["CORS_HEADERS"] = "Content-Type"


@app.route("/", methods=["GET"])
def index():
    return render_template(
        "index.html", service_url="http://{}:{}/ttsb64".format(SERVICE_HOST, PORT)
    )


@app.route("/tts", methods=["POST"])
def tts():
    sentence = request.json["sentence"]
    print(f"The input sentece is {sentence}")
    wav = generate_wav(sentence, save=False, b64=False)
    response = make_response(wav)
    response.headers["Content-Type"] = "audio/wav"
    response.headers["Content-Disposition"] = "attachment; filename=sound.wav"
    response.status = 200

    return response


@app.route("/ttsb64", methods=["POST"])
def tts64():
    sentence = request.json["sentence"]
    print(f"The input sentece is {sentence}")
    b64wav = generate_wav(sentence, save=False, b64=True)
    response = app.response_class(
        response=json.dumps(b64wav), status=200, mimetype="application/json"
    )

    return response
    # sentence = unicodedata.normalize("NFD", sentence)
    # path_to_wav = generate_wav(sentence)

    # print(f"The exported path to the wav file is {path_to_wav}")
    # return send_file(path_to_wav,
    #                 mimetype="audio/wav",
    #                 as_attachment=True,
    #                 attachment_filename="test.wav",
    #                 cache_timeout=0)
    # corrected = chunk_spell_correct(sentence)  # spell_correct([sentence])

    # return jsonify({"wav": wav})


if __name__ == "__main__":
    from waitress import serve

    serve(app, host="0.0.0.0", port=PORT)
