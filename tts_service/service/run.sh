
SCRIPT_PATH="$(dirname $(readlink -f $0))"

#USER_DIR=/taco/tts_service
USER_DIR=$(pwd)

SERVICE_HOST=${SERVICE_HOST:-0.0.0.0}
SERVICE_PORT=${SERVICE_PORT:-5000}
CONFIG_FILE="config.json"
echo "Running on http://$SERVICE_HOST:$SERVICE_PORT"
echo "script path is ${SCRIPT_PATH}"


python $SCRIPT_PATH/predict.py \
    --webhost $SERVICE_HOST \
    --webport $SERVICE_PORT \
    --config $CONFIG_FILE \
    --user_dir $USER_DIR
