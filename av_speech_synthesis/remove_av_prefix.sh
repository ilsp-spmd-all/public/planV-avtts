# scripts which removes prefix and extra_suffix from
# a given filename and appends desired_suffix in the end
prefix="av_"
extra_suffix=".wav.mp4"
desired_suffix=".mp4"
for k in xls_template2_all/*;
do
    old_name=$(basename $k)
    new_name=${old_name#"$prefix"}
    new_name=${new_name%"$extra_suffix"}
    new_name=${new_name}${desired_suffix}
    #echo $new_name
    mv $k xls_template2_all/$new_name
done
