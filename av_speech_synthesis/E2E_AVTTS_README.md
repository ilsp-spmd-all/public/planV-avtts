# End to end AudioVisual Speech Synthesis

1.  Login to remote machine

    ```bash
    # command from local machine
    # login via sudo as user: efthygeo
    ssh diaoque3
    ```
2. Go to project folder

    ```bash
    cd planv_tts
    # includes
    # (https://gitlab.com/ilsp-spmd-all/planv/planv-generic) --> repo
    ```

3. Setup venv if does not exist, activate otherwise
    ```
    virualenv -p python3 planv_tts_venv
    pip3 install -r requirements.txt
    ```

4. Create folder for every csv template and move .csv file in the folder
    ```bash
    cd planv-generic/synthesize_avatar_voice
    mkdir template21
    cp template21.csv template21
    ### Tip: sed -i 's/;/,/g' 1_5_verbs_instructions.csv
    python3 synthesize_from_csv.py template21/template21.csv
    ```

5. Download localy and upload to `mp3s_to_be_synthesized`
    ```bash
    # command from local machine
    scp -r  dialoque3:/home/efthygeo/planv_tts/planv-generic/synthesize_avatar_voice/templates1_5  ~/Documents/Athena_RC/projects/planV-templates/mp3/

    ```

6. Prepare audio for avatar network: Convert the synthesized clips into 16kHz and append 1s of silene in the end. Also convert to `.wav`
    ```
    bash downsample_pad_wav.sh planv-generic/synthesize_avatar_voice/templates1_5/1_5_verbs_instructions
    ```

7. Login to `efthymis` account in tesla
   ```bash
   scp -r dialoque3:/home/efthygeo/planv_tts/planv-generic/synthesize_avatar_voice/templates1_5_wav /home/efthymis/avatar/data
   ```
8. Physically access tesla at AthenaRC office 
   1. Login as `efthymis`
   2. Activate `avatar` conda env
   3. ```cd avatar/av-speech-synthesis```
   4. modify `run_sep_22.sh`
   5. (Optional) `$ export CUDA_VISIBLE_DEVICES=-1`
   6. `$ bash run_sep_22.sh`

## CSV template

- col1 (mp3_filename) ```filename.mp3```
- col2: target

## Uploading
The `.zip` folder name should be identical to the `.csv` name and be uploaded to

```
Plan-V/Documents/Implementation/WP1_speech_language_therapy_protocols/Finalised_excercises/avatar_mp4s/mp3s_to_be_synthesized
``` 
After converting to avtar `.mp4` move to `avatar_mp4s/mp3s`


## Issue for glsw in docker
- https://unix.stackexchange.com/questions/564789/running-glfw-in-docker

- make window invisible: 'https://stackoverflow.com/questions/66299684/how-to-prevent-glfw-window-from-showing-up-right-in-creating'
- headless rendering: 'http://www.open3d.org/docs/latest/docker.html'
- remote GUI: 'https://www.scm.com/doc/Installation/Remote_GUI.html'

## backdoor fix 

- ```bash
  sudo apt install xvfb xorg-dev
  ```
- ```bash 
  xvfb-run python ....
  ```


# Setup Docker
- ```bash
  docker build . --rm -t facewarp
  
  docker run -ti -v $PWD:/workspace/avatar/ -v /home/egeorgiou/avtts/facewarp_linux/:/workspace/avatar/facewarp_linux -v /home/egeorgiou/my_data/leftovers:/workspace/avatar/leftovers_audio facewarp

  xvfb-run python main_end2end_cartoon_linux.py --jpg cartoonM.png --jpg_bg cartoonM_bg.jpg --output_folder leftovers_docker --audio_folder leftovers_audio --cpu
  ```

