# Guide for compiling *opencv-python* with CUDA

> Tested in the asimov workstation

### Userful Guides

1.    opencv installation in ubuntu (outdated): [link](https://www.pyimagesearch.com/2016/07/11/compiling-opencv-with-cuda-support/) 
2.    [install libjasper](https://github.com/opencv/opencv/issues/8622>)
3.    Optional [Issue Fix](https://github.com/NVIDIA/nvidia-container-runtime/issues/13) with nvidia-container-runtime.list
4.    ```chown``` command needs name change from ```-R ubuntu``` to `-R efthygeo`, i.e. your username
5.   Also did remove `pip cache` via `pip cache purge`
6.   Added the following lines in the end of my `~/.zshrc` (or `~/.bashrc`)
```bash
export WORKON_HOME=$HOME/.virtualenvs
export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python3
export VIRTUALENVWRAPPER_VIRTUALENV=/usr/local/bin/virtualenv
source /usr/local/bin/virtualenvwrapper.sh
```
7.  ```bash  
    cmake -D CMAKE_BUILD_TYPE=RELEASE \
    -D CMAKE_INSTALL_PREFIX=/home/efthygeo/opt/opencv \
    -D OPENCV_GENERATE_PKGCONFIG=ON \
    -D BUILD_EXAMPLES=OFF \
    -D INSTALL_PYTHON_EXAMPLES=OFF \
    -D INSTALL_C_EXAMPLES=OFF \
    -D PYTHON_DEFAULT_EXECUTABLE=$(which python3) \
    -D BUILD_opencv_python3=ON -D BUILD_NEW_PYTHON_SUPPORT=ON -D HAVE_opencv_python3=ON \
    -D PYTHON3_EXECUTABLE=$(which python3) \
    -D PYTHON3_INCLUDE_DIR=$(python3 -c "from distutils.sysconfig import get_python_inc; print(get_python_inc())") \
    -D PYTHON3_PACKAGES_PATH=$(python3 -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())") \
    -D ENABLE_FAST_MATH=1 -D CUDA_FAST_MATH=1 -D WITH_CUBLAS=1 -D WITH_CUDA=ON -D BUILD_opencv_cudacodec=OFF -D WITH_CUDNN=ON -D OPENCV_DNN_CUDA=ON -D WITH_V4L=ON -D WITH_QT=OFF -D OPENCV_GENERATE_PKGCONFIG=ON -D OPENCV_PC_FILE_NAME=opencv.asimov -D OPENCV_ENABLE_NONFREE=ON -D OPENCV_EXTRA_MODULES_PATH=/home/efthygeo/src/opencv_contrib-4.5.5/modules -D BUILD_opencv_world=ON -D BUILD_opencv_contrib_world=ON -D CUDA_TOOLKIT_ROOT_DIR=/usr/lib/cuda-11.2 ..
    ```