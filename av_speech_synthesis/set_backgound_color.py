import os
import sys
import skimage
import numpy as np
from skimage import io



source_image_path = sys.argv[1] # for example 'roy_example.png'
custom_image = io.imread(source_image_path)

bg_color = custom_image[0, 0, :]
H, W, C= custom_image.shape[0], custom_image.shape[1], custom_image.shape[2]
bg_image = np.ones((H, W, C), dtype=custom_image.dtype) * bg_color
path_to_cartoon = source_image_path.split("/")[0]
cartoon_name = source_image_path.split("/")[1]
if ".jpg" in cartoon_name:
    suffix = ".jpg"
elif ".png" in cartoon_name:
    suffix = ".png"
else:
    print("Not a valid image suffix")

cartoon_name = cartoon_name.strip(suffix)
bg_image_name = cartoon_name + "_bg" + suffix
io.imsave(os.path.join(path_to_cartoon, bg_image_name), bg_image)
print(f"Succesfully stored the background image under {path_to_cartoon} folder with name {bg_image_name}")

