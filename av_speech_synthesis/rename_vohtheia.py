import os
import sys
import tqdm
import shutil

TYPE_DICT = [
    {
        "Synonyma": 3,
        "Antonyma": 4,
    },
    {
        "Rimata": 2,
        "Epirrimata": 4,
        "Epi8eta": 3
    },
]

def check_title_vohtheia(title):
    "check for keyword and return _vohtheia*_ id"
    if "vohtheia" in title:
        title_list = []
        title_list.append(title.split("_")[1])
        title_list.append(title.split("_")[-1].split(".")[0])
        return title_list
    else:
        return ""

def check_title_rhmata(title):
    "check for keyword and return _vohtheia*_ id"
    if "rhmata" in title:
        title_list = []
        title_list.append(title.split("_")[1])
        title_list.append("rhmata")
        return title_list
    else:
        return ""

def check_title_epitheta(title):
    "check for keyword and return _vohtheia*_ id"
    if "epitheta" in title:
        title_list = []
        title_list.append(title.split("_")[1])
        title_list.append("epitheta")
        return title_list
    else:
        return ""

def check_title_epirrhmata(title):
    "check for keyword and return _vohtheia*_ id"
    if "epirrhmata" in title:
        title_list = []
        title_list.append(title.split("_")[1])
        title_list.append("epirrhmata")
        return title_list
    else:
        return ""

def get_type(s, id=0):
    "get the type id from the TYPE_DICT"
    s_type_dict = TYPE_DICT[id]
    for k, v in s_type_dict.items():
        if k in s:
            return str(v)


if __name__ == "__main__":
    folder_path = sys.argv[1]
    id_0 = get_type(folder_path.split("/")[0], id=0)
    id_1 = get_type(folder_path.split("/")[0], id=1)
    id_2 = "0"
    all_mp4s = os.listdir(os.path.join(folder_path, "all_mp4s"))
    for av_mp4 in tqdm.tqdm(all_mp4s):
        id_3 = check_title_epirrhmata(av_mp4)
        if isinstance(id_3, list):
            print("renaming epirrhmata file")
            from_path = os.path.join(folder_path, "all_mp4s", av_mp4)
            to_path = os.path.join(
                folder_path,
                "all_mp4s",
                f"{id_0}_{id_1}_{id_2}_{id_3[1]}_{id_3[0]}.wav.mp4"
                )
            shutil.move(from_path, to_path)
            # shutil.copy(from_path, to_path)