MP4_FOLDER=$1
mkdir -p $MP4_FOLDER/all_mp4s
for k in $MP4_FOLDER/*;
do
    for anime in $k/*.mp4;
    do
        mv $anime ${MP4_FOLDER}/all_mp4s
    done
done
