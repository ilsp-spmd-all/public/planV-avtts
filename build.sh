docker build -f Dockerfile -t kripis_service \
    --build-arg I2I_MODEL=ckpt_116_i2i_comb.pth \
    --build-arg AUTOVC_MODEL=ckpt_autovc.pth \
    --build-arg MKIT_CONTENT_MODEL=ckpt_content_branch.pth \
    --build-arg MKIT_SPEAKER_MODEL=ckpt_speaker_branch.pth .
    # --build-arg SERVICE_HOST=192.168.188.156 \
    # --build-arg SERVICE_PORT=6006 \
    # --build-arg TACO_MODEL=tacotron2.pt \
    # --build-arg GLOW_MODEL=waveglow.pt .
