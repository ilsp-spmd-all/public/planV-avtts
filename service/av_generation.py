import os
import argparse
import json
from infer import make_tts_fn
from infer import parse_args as parse_args_taco
from utils import parse_args_kripis


def read_config(cf_path):
    "reads the configuration for the models and all available arguments"
    with open(os.path.join("en-config.json")) as fd:
        args = json.load(fd)
    return args


if __name__ == "__main__":
    # read configuration file
    taco_args = read_config
    # parse input arguments
    parser = argparse.ArgumentParser(description="TACOTRON2 TTS SERVICE")
    # split parsing to avoid name conflicts
    parser = parse_args_taco(parser)
    generate_wav = make_tts_fn(
        parser,
        taco_args["tacotron2"],
        taco_args["waveglow"],
        taco_args["text_cleaners"],
        taco_args["sigma_infer"],
        taco_args["denoising_strength"],
        taco_args["stft_hop_length"],
        taco_args["cpu"],
        taco_args["fp16"],
        taco_args["sr"],
        user_path="av-examples",
    )

    # example sentence
    text_in = "Hello darkness my old friend, i come to talk with you again."
