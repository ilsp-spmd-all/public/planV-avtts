#!/bin/bash

# PYTHONPATH=${PYTHONPATH}:/avatar
USER_DIR="$(pwd)"
# Get the script path
SCRIPT_PATH="$(dirname "$(readlink -f "$0")")"

# Set the service host and port
SERVICE_HOST="${1:-0.0.0.0}"
SERVICE_PORT="${2:-5000}"

# Set the path to the config file
CONFIG_FILE="workspace/avatar/service/config.json"

echo "Running on http://$SERVICE_HOST:$SERVICE_PORT"
echo "Script path is $SCRIPT_PATH"

# Set the Python path
PYTHONPATH=${PYTHONPATH}:/workspace/avatar

# Execute the python script
xvfb-run python "$SCRIPT_PATH/predict.py" \
    --webhost "$SERVICE_HOST" \
    --webport "$SERVICE_PORT" \
    --configs "$CONFIG_FILE" \
    --user_dir "$USER_DIR"
