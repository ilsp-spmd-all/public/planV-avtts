#!/bin/bash

docker run --shm-size=1g --ulimit memlock=-1 --ulimit stack=67108864 -it --rm --ipc=host -v $PWD:/workspace/avatar -v /home/egeorgiou/my_data/leftovers:/workspace/avatar/leftovers_audio kripis_service bash
