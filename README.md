# Kripis AV Synthesis Pipeline

## Download pre-trained models
#### MakeItTalk
From the README.md file in `av_speech_synthesis` folder we get the pre-trained models in the following links

| Model |  Link to the model | 
| :-------------: | :---------------: |
| Voice Conversion  | [Link](https://drive.google.com/file/d/1ZiwPp_h62LtjU0DwpelLUoodKPR85K7x/view?usp=sharing)  |
| Speech Content Module  | [Link](https://drive.google.com/file/d/1r3bfEvTVl6pCNw5xwUhEglwDHjWtAqQp/view?usp=sharing)  |
| Speaker-aware Module  | [Link](https://drive.google.com/file/d/1rV0jkyDqPW-aDJcj7xSO6Zt1zSXqn1mu/view?usp=sharing)  |
| Image2Image Translation Module  | [Link](https://drive.google.com/file/d/1i2LJXKp-yWKIEEgJ7C6cE3_2NirfY_0a/view?usp=sharing)  |
| Non-photorealistic Warping (.exe)  | [Link](https://drive.google.com/file/d/1rlj0PAUMdX8TLuywsn6ds_G6L63nAu0P/view?usp=sharing)  | 

Store them under the `planV-avtts` folder.

Download pre-trained embedding [here] and save to `av_speech_synthesis/examples/dump` folder.
> Comment: In the initial repo they were stored under `av_speech_synthesis/examples/ckpt`

#### TTS
- Tacotron-2 (English) [here](https://ngc.nvidia.com/catalog/models/nvidia:tacotron2pyt_fp16/files?version=3)
- WaveGlow (English) [here](https://ngc.nvidia.com/catalog/models/nvidia:waveglow256pyt_fp16/files?version=2)
- Regotron (Greek) [here]()
- WaveGlow (Greek) [here]()
Store them under the `planV-avtts` folder.



## Text to Kripis standalone Pipeline
This may be used if you want to setup the text-to-avatar pipeline at your own machine
1. Create a(n) (conda) environment and build the image.
2. Store the desired .txt under `av_requests/text`
3. `bash interactive.sh`
4. `xvfb-run python text2kripis.py`

## Text to Kripis Service Pipeline
This should be used in order to setup the whole text-to-avatar pipeline as a service.

**Step1** Build the image, which is universal image for english/greek AVTTS as well as
audio2kripis service
```bash
bash build.sh
```
**Step2** Run the container with name `en_kripis_service` with container and localhost port 5000
and the machine's external ip, e.g., `192.168.188.156`
```bash
docker run -p 5000:5000 --name en_kripis_service --rm -it -e PYTHONPATH=$PYTHONPATH:/avatar --entrypoint /bin/bash kripis_service /workspace/avatar/service/run.sh 192.168.188.156 5000
```
**Step3** Access the service via its endpoint either from the UI (frontend) in `http://192.168.188.156:5000/`
or via a curl command from command line
```bash
curl -X POST http://192.168.188.156:5000/kripis  -H 'Content-Type: application/json' -d '{"sentence": "Hello darkeness my old friend. I come to talk with you again."}' --output out.mp4
```

## Additional Services
- English (EN) text-to-avatar pipeline under `service/run.sh`
- Audio-to-avatar pipeline under `service_audio/run.sh` where you may upload a recorded .wav (both english and greek have been tested) file and generate a talking head. You may also send files to the endpoint via curl as 
```bash
curl -X 'POST' 'http://192.168.188.151:5000/predict' -H 'accept: application/json' -H 'Content-Type: multipart/form-data' -F _input=@A_Caminhada_dos_Elefantes_short.wav;type=audio/wav
```
- Greek (GR) text-to-avatar pipeline under `service_gr/run.sh` 

All services utilize the same docker image and may therefore be executed in the exact same way by only altering the `--entrypoint` argument to the desired `/worskspace/avatar/<which_service>/run.sh`

# API Documentation

1. Enter the desired text into the blue box displayed in the figure. The text will be synthesized in English. Ensure accurate spelling and include a period at the end.
2. Click the Generate Avatar button highlighted in the red oval. This will generate a talking avatar below. You can adjust the display volume, screen settings, and directly download the avatar.

![AV_Kripis_Service](kripis_service_anno.png)