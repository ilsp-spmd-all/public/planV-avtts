FROM python:3.6.13-buster
#FROM ubuntu:20.04
# Use ubuntu (~64MB) vs python (~934MB)

# CPU version is much smaller. Change this if you need GPU

# ARG SERVICE_HOST=0.0.0.0
# ARG SERVICE_PORT=5000
ARG TORCH_VERSION=1.6.0+cpu
ARG NUMPY_VERSION=1.18.1
ARG SCIPY_VERSION=1.4.1
ARG LIBROSA_VERSION=0.6.3
ARG NUMBA_VERSION=0.48
ARG PYSPTK_VERSION=0.1.18
### old-comments-start ###
## ARG FAIRSEQ_COMMIT=9ae39465daff48650ed7956adf2980a697016d50
## ARG CHECKPOINT_DIR=./checkpoints
## ARG SPELL_LARGE=spell-gr-large
## ARG SPELL_SMALL=spell-gr-small
### old-comments-end ###
# ARG TACO_MODEL
# ARG GLOW_MODEL
ARG I2I_MODEL
ARG AUTOVC_MODEL
ARG MKIT_CONTENT_MODEL
ARG MKIT_SPEAKER_MODEL

# RUN apt-get update

# from old taco dockerfile
#ADD . /taco/tts_service
#WORKDIR /taco/tts_service
# RUN pip install --no-cache-dir -r requirements.txt
# ENV LD_PRELOAD=/usr/lib/x86_64-linux-gnu/libtcmalloc_minimal.so.4

RUN apt-get update -y && \
    apt-get install -y libsndfile1 && \
    apt-get install -y build-essential git && \
    apt-get install -y xvfb xorg-dev && \
    apt-get install -y ffmpeg && \
    apt-get install -y sox && \
    pip install --no-cache-dir --upgrade pip setuptools wheel pip && \
    pip install --no-cache-dir torch==${TORCH_VERSION} -f https://download.pytorch.org/whl/torch_stable.html && \
    pip install --no-cache-dir flask flask_cors joblib numpy==${NUMPY_VERSION} scipy==${SCIPY_VERSION} waitress unidecode inflect librosa==${LIBROSA_VERSION} setuptools numba==${NUMBA_VERSION} && \
    pip install --no-cache-dir ffmpeg-python opencv-python==4.4.0.46 face_alignment scikit-learn pydub pynormalize soundfile pyworld resemblyzer pysptk==${PYSPTK_VERSION} && \
    apt-get remove --purge -y build-essential gcc g++ python-pip git && \
    #apt-get remove --purge -y build-essential gcc g++ python3-pip git && \
    apt-get autoremove -y && \
    apt-get clean && \
    apt-get autoclean && \
    rm -rf /var/lib/apt/lists/* && \
    ls -l /tmp/ && \
    rm -rf /tmp/*

# # Run copy after dependencies are installed to avoid rebuilding every time we make changes to the code.
# RUN mkdir -p /taco/tts_service/
# WORKDIR /taco/tts_service/
# # COPY ./ /fairseq_user_dir/examples/spell_corrector/
# COPY ${TACO_MODEL} /taco/tts_service/tacotron2.pt
# COPY ${GLOW_MODEL} /taco/tts_service/waveglow.pt
ADD . /workspace/avatar
WORKDIR /workspace/avatar

COPY ${I2I_MODEL} /workspace/avatar/av-speech-synthesis/examples/ckpt/
COPY ${AUTOVC_MODEL} /workspace/avatar/av-speech-synthesis/examples/ckpt/
COPY ${MKIT_CONTENT_MODEL} /workspace/avatar/av-speech-synthesis/examples/ckpt/
COPY ${MKIT_SPEAKER_MODEL} /workspace/avatar/av-speech-synthesis/examples/ckpt/

# # Explicitly copy only source files....
# COPY phrases.txt /taco/tts_service/
# COPY models.py /taco/tts_service/
# COPY tacotron2/ /taco/tts_service/tacotron2/
# COPY waveglow/ /taco/tts_service/waveglow/
# COPY common/ /taco/tts_service/common/
# COPY requests/ /taco/tts_service/requests/
# COPY service/ /taco/tts_service/service/

#### old commands - start ####
# COPY __init__.py /fairseq_user_dir/examples/spell_corrector/
# COPY models/ /fairseq_user_dir/examples/spell_corrector/models/
# COPY predictor.py /fairseq_user_dir/examples/spell_corrector/
# COPY tasks/ /fairseq_user_dir/examples/spell_corrector/tasks/
# COPY website/ /fairseq_user_dir/examples/spell_corrector/website/

#COPY ${CHECKPOINT_DIR}/${SPELL_LARGE}.pt /checkpoints/${SPELL_LARGE}.pt
#COPY ${CHECKPOINT_DIR}/${SPELL_SMALL}.pt /checkpoints/${SPELL_SMALL}.pt
#### old commands - end ####

# # Copy only one model. Requires --build-arg MODEL=/host/path/to/model
# RUN touch /taco/tts_service/__init__.py

# COPY my_infer.py /taco/tts_service/
# COPY custom_infer.py /taco/tts_service/
# COPY infer.py /taco/tts_service/
# COPY config.json /taco/tts_service/

#### old commands - start ####
# uncomment the following in case of using FROM:ubuntu``
#RUN ln -s $(which python3) /usr/bin/python
#### old commands - end ####

# ENV SERVICE_HOST=$SERVICE_HOST
# ENV SERVICE_PORT=$SERVICE_PORT

#### old commands - start ####
# ENV SPELL_CONFIG=${SPELL_SMALL}
# ENV SPELL_CONFIG=model
#### old commands - end ####

# ENV PYTHONPATH=${PYTHONPATH}:/avatar
# ENV PYTHONPATH=${PYTHONPATH}:/taco

# ENTRYPOINT ["/bin/bash", "/workspace/avatar/service/run.sh"]
