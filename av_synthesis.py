import argparse
import glob
import os
import pickle
import shutil
import uuid
import time

import numpy as np


def parse_args_kripis(parser):
    """
    Parse commandline arguments for Kripis.
    """
    parser.add_argument(
        "--jpg",
        type=str,
        required=False,
        help="Puppet image name to animate (with filename extension), e.g. wilk.png",
        default="cartoonM.png",
    )
    parser.add_argument(
        "--jpg_bg",
        type=str,
        required=False,
        help="Puppet image background (with filename extension), e.g. wilk_bg.jpg",
        default="cartoonM_bg.jpg",
    )
    parser.add_argument(
        "--inner_lip",
        default=False,
        action="store_true",
        help="add this if the puppet is created with only inner lip landmarks",
    )

    parser.add_argument("--out", type=str, default="out.mp4")

    parser.add_argument(
        "--load_AUTOVC_name",
        type=str,
        default="/workspace/avatar/av_speech_synthesis/examples/ckpt/ckpt_autovc.pth",
    )
    parser.add_argument(
        "--load_a2l_G_name",
        type=str,
        default="/workspace/avatar/av_speech_synthesis/examples/ckpt/ckpt_speaker_branch.pth",
    )  # ckpt_audio2landmark_g.pth') #
    parser.add_argument(
        "--load_a2l_C_name",
        type=str,
        default="/workspace/avatar/av_speech_synthesis/examples/ckpt/ckpt_content_branch.pth",
    )  # ckpt_audio2landmark_c.pth')
    parser.add_argument(
        "--load_G_name",
        type=str,
        default="/workspace/avatar/av_speech_synthesis/examples/ckpt/ckpt_116_i2i_comb.pth",
    )  # ckpt_i2i_finetune_150.pth') #ckpt_image2image.pth') #

    parser.add_argument("--amp_lip_x", type=float, default=2.0)
    parser.add_argument("--amp_lip_y", type=float, default=2.0)
    parser.add_argument("--amp_pos", type=float, default=0.5)
    parser.add_argument(
        "--reuse_train_emb_list", type=str, nargs="+", default=[]
    )  #  ['E_kmpT-EfOg']) #  ['E_kmpT-EfOg']) # ['45hn7-LXDX8'])

    parser.add_argument("--add_audio_in", default=False, action="store_true")
    parser.add_argument("--comb_fan_awing", default=False, action="store_true")
    parser.add_argument("--output_folder", type=str, default="/workspace/avatar/av_requests")
    parser.add_argument("--audio_folder", type=str, default="audio_folder")
    parser.add_argument(
        "--dump_folder",
        type=str,
        default="/workspace/avatar/av_requests",
    )

    #### NEW POSE MODEL
    parser.add_argument("--test_end2end", default=True, action="store_true")
    parser.add_argument("--dump_dir", type=str, default="", help="")
    parser.add_argument("--pos_dim", default=7, type=int)
    parser.add_argument("--use_prior_net", default=True, action="store_true")
    parser.add_argument("--transformer_d_model", default=32, type=int)
    parser.add_argument("--transformer_N", default=2, type=int)
    parser.add_argument("--transformer_heads", default=2, type=int)
    parser.add_argument("--spk_emb_enc_size", default=16, type=int)
    parser.add_argument("--init_content_encoder", type=str, default="")
    parser.add_argument("--lr", type=float, default=1e-3, help="learning rate")
    parser.add_argument("--reg_lr", type=float, default=1e-6, help="weight decay")
    parser.add_argument("--write", default=False, action="store_true")
    parser.add_argument(
        "--segment_batch_size", type=int, default=512, help="batch size"
    )
    parser.add_argument("--emb_coef", default=3.0, type=float)
    parser.add_argument("--lambda_laplacian_smooth_loss", default=1.0, type=float)
    parser.add_argument("--use_11spk_only", default=False, action="store_true")
    parser.add_argument("--cent_face", default=False, action="store_true")
    parser.add_argument("--close_mouth_ratio", default=0.99, type=float)
    # parser.add_argument("--cpu", default=False, action="store_true")

    return parser


def make_kripis_fn(
    parser,
    i2i,
    autovc,
    spk_branch,
    content_branch,
    amp_lip_x=2.0,
    amp_lip_y=2.0,
    amp_pos=0.5,
    output_folder="/workspace/avatar/av_requests",
    input_folder="/workspace/avatar/av_requests",
    # dump_folder="/workspace/avatar/av_speech_synthesis/tmp/dump",
    cpu=True,
):
    from av_speech_synthesis.thirdparty.resemblyer_util.speaker_emb import get_spk_emb
    from av_speech_synthesis.src.autovc.AutoVC_mel_Convertor_retrain_version import (
        AutoVC_mel_Convertor,
    )
    from av_speech_synthesis.src.approaches.train_audio2landmark import (
        Audio2landmark_model,
    )

    DEMO_CH = parser.jpg.split(".")[0]
    shape_3d = np.loadtxt(
        f"/workspace/avatar/av_speech_synthesis/examples_cartoon/{DEMO_CH}_face_close_mouth.txt"
    )
    centerize_face = parser.cent_face


    def fix_pickles(au_data_i, dump_folder):
        fl_data = []
        rot_tran, rot_quat, anchor_t_shape = [], [], []

        for au, info in au_data_i:
            au_length = au.shape[0]
            fl = np.zeros(shape=(au_length, 68 * 3))
            fl_data.append((fl, info))
            rot_tran.append(np.zeros(shape=(au_length, 3, 4)))
            rot_quat.append(np.zeros(shape=(au_length, 4)))
            anchor_t_shape.append(np.zeros(shape=(au_length, 68 * 3)))
        # au, info = au_data_i[0], au_data_i[1]
        # au_length = au.shape[0]
        # fl = np.zeros(shape=(au_length, 68 * 3))
        # fl_data.append((fl, info))
        # rot_tran.append(np.zeros(shape=(au_length, 3, 4)))
        # rot_quat.append(np.zeros(shape=(au_length, 4)))
        # anchor_t_shape.append(np.zeros(shape=(au_length, 68 * 3)))

        ### this part is normally executed
        # if os.path.exists(os.path.join(dump_folder, "random_val_fl.pickle")):
        #     os.remove(os.path.join(dump_folder, "random_val_fl.pickle"))

        # if os.path.exists(os.path.join(dump_folder, "random_val_fl_interp.pickle")):
        #     os.remove(os.path.join(dump_folder, "random_val_fl_interp.pickle"))

        # if os.path.exists(os.path.join(dump_folder, "random_val_au.pickle")):
        #     os.remove(os.path.join(dump_folder, "random_val_au.pickle"))

        # if os.path.exists(os.path.join(dump_folder, "random_val_gaze.pickle")):
        #     os.remove(os.path.join(dump_folder, "random_val_gaze.pickle"))

        # store the new pickles in tmp/dump/ folder
        with open(os.path.join(dump_folder, "random_val_fl.pickle"), "wb") as fp:
            pickle.dump(fl_data, fp)
        with open(os.path.join(dump_folder, "random_val_au.pickle"), "wb") as fp:
            pickle.dump(list(au_data_i), fp)
        with open(os.path.join(dump_folder, "random_val_gaze.pickle"), "wb") as fp:
            gaze = {
                "rot_trans": rot_tran,
                "rot_quat": rot_quat,
                "anchor_t_shape": anchor_t_shape,
            }
            pickle.dump(gaze, fp)

    def generate_kripis(wav_name, save_id,
                        out_folder="/workspace/avatar/av_requests",
                        save=True):
        output_folder = os.path.join(out_folder, save_id)
        input_folder = os.path.join(out_folder, save_id)
        dump_folder = os.path.join(output_folder, 'dump')
        os.mkdir(dump_folder)
        print(f"I/O folder is {output_folder}")
        print(f"It contains {os.listdir(input_folder)}")

        print("------CHECKPOINT #1 --------")
        # check audio file
        # os.system(
        #     f"ffmpeg -y -loglevel error -i {input_folder}/{wav_name} -ar 16000 {input_folder}/tmp.wav"
        # )
        # print("Done downsampling")
        # shutil.copyfile(
        #     f"{input_folder}/tmp.wav",
        #     f"{input_folder}/{wav_name}",
        # )

        # check audio file
        # os.system(
        #     f"ffmpeg -y -loglevel error -i {input_folder}/{wav_name} -ar 16000 {input_folder}/tmp.wav"
        # )
        # print("Done downsampling")
        # shutil.copyfile(
        #     f"{input_folder}/tmp.wav",
        #     f"{input_folder}/{wav_name}",
        # )

        print("------CHECKPOINT #2 --------")

        # get speaker embedding
        me, ae = get_spk_emb(
            f"{input_folder}/{wav_name}", use_cpu=cpu
        )
        print(f"{input_folder}/{wav_name}")
        au_emb = [me.reshape(-1)]
        c = AutoVC_mel_Convertor(input_folder, use_cpu=cpu)
        au_data_i = c.convert_single_wav_to_autovc_input(
            audio_filename=os.path.join(input_folder, wav_name),
            autovc_model_path=autovc,
        )

        # print("SLEEPING START ---------------------")
        # time.sleep(10)
        # print("-------------------SLEEPING END")

        print(f"{input_folder}")
        # remove tmp.wav

        if os.path.isfile(f"{input_folder}/tmp.wav"):
            os.remove(f"{input_folder}/tmp.wav")

        # initialize/remove old/store new pickles
        fix_pickles(list(au_data_i), dump_folder)

        face_offset = None
        # reset output folder
        parser.output_folder = output_folder
        parser.dump_folder = dump_folder
        model = Audio2landmark_model(
            parser,
            jpg_shape=shape_3d,
            face_offset=None,
            close_mouth_ratio=parser.close_mouth_ratio,
        )

        if centerize_face:
            face_offset = np.array([0.0, 0.8, 0.0])

        if len(parser.reuse_train_emb_list) == 0:
            print("ABRA")
            # print(f"len of embed is {len(au_emb)}")
            model.test(au_emb=au_emb, centerize_face=centerize_face)
        else:
            print("SABRA")
            model.test(au_emb=None, centerize_face=centerize_face)

        ### fls
        fls_names = glob.glob1(output_folder, "pred_fls_*.txt")
        fls_names.sort()
        print(f"fls_names is {fls_names}")
        print(os.listdir(output_folder))
        print(f"The name of the output folder is {output_folder}")
        print(f"The current directory {os.getcwd()} has {os.listdir(os.getcwd())}")
        print(f"The directory '/workspace/avatar/av_requests/' has {os.listdir('/workspace/avatar/av_requests/')}")

        ains = glob.glob1(input_folder, "*.wav")
        # import pdb; pdb.set_trace()
        ains.sort()

        print("------CHECKPOINT #3 --------")
        print(f"The length of the total fls_names is {len(fls_names)}")
        for i in range(0, len(fls_names)):
            # time.sleep(0.1)
            print(f"The length of the ains is {len(ains)}")
            ain = ains[i]
            fl = np.loadtxt(os.path.join(output_folder, fls_names[i])).reshape(
                (-1, 68, 3)
            )
            # tmp_output_dir = os.path.join(output_folder, fls_names[i][:-4])
            output_dir = os.path.join(output_folder, fls_names[i][:-4])
            print(f"Output dir is {output_dir}")
            try:
                os.makedirs(output_dir)
                print("succesfully created the directory")
            except:
                pass

            from av_speech_synthesis.util.utils import get_puppet_info

            bound, scale, shift = get_puppet_info(
                DEMO_CH,
                ROOT_DIR="/workspace/avatar/av_speech_synthesis/examples_cartoon",
            )
            print("------CHECKPOINT #3b --------")

            fls = fl.reshape((-1, 68, 3))

            fls[:, :, 0:2] = -fls[:, :, 0:2]
            fls[:, :, 0:2] = fls[:, :, 0:2] / scale
            fls[:, :, 0:2] -= shift.reshape(1, 2)

            fls = fls.reshape(-1, 204)

            # additional smooth
            from scipy.signal import savgol_filter

            fls[:, 0 : 48 * 3] = savgol_filter(fls[:, 0 : 48 * 3], 17, 3, axis=0)
            fls[:, 48 * 3 :] = savgol_filter(fls[:, 48 * 3 :], 11, 3, axis=0)
            fls = fls.reshape((-1, 68, 3))

            # if (DEMO_CH in ['paint', 'mulaney', 'cartoonM', 'beer', 'color', 'JohnMulaney', 'vangogh', 'jm', 'roy', 'lineface', 'makis']):

        if not parser.inner_lip:
            r = list(range(0, 68))
            fls = fls[:, r, :]
            fls = fls[:, :, 0:2].reshape(-1, 68 * 2)
            fls = np.concatenate((fls, np.tile(bound, (fls.shape[0], 1))), axis=1)
            fls = fls.reshape(-1, 160)

        else:
            r = list(range(0, 48)) + list(range(60, 68))
            fls = fls[:, r, :]
            fls = fls[:, :, 0:2].reshape(-1, 56 * 2)
            fls = np.concatenate((fls, np.tile(bound, (fls.shape[0], 1))), axis=1)
            fls = fls.reshape(-1, 112 + bound.shape[1])

        np.savetxt(os.path.join(output_dir, "warped_points.txt"), fls, fmt="%.2f")

        print("------CHECKPOINT #4 --------")

        # static_points.txt
        static_frame = np.loadtxt(
            os.path.join(
                "/workspace/avatar/av_speech_synthesis/examples_cartoon",
                "{}_face_open_mouth.txt".format(DEMO_CH),
            )
        )
        static_frame = static_frame[r, 0:2]
        static_frame = np.concatenate((static_frame, bound.reshape(-1, 2)), axis=0)
        np.savetxt(
            os.path.join(output_dir, "reference_points.txt"), static_frame, fmt="%.2f"
        )

        # triangle_vtx_index.txt
        shutil.copy(
            os.path.join(
                "/workspace/avatar/av_speech_synthesis/examples_cartoon",
                DEMO_CH + "_delauney_tri.txt",
            ),
            os.path.join(output_dir, "triangulation.txt"),
        )

        # TODO
        # uncomment
        # os.remove(os.path.join(output_folder, fls_names[i]))

        # ==============================================
        # Step 4 : Vector art morphing
        # ==============================================
        warp_exe = os.path.join(
            os.getcwd(), "av_speech_synthesis", "facewarp", "facewarp.exe"
        )
        print("------CHECKPOINT #5 --------")
        print("Initializing Vector Morphing")
        # warp_linux = os.path.join(os.getcwd(), 'facewarp', 'linux_facewarp')
        ### via temrinal
        # warp_linux = '/home/egeorgiou/avtts/facewarp_linux/MakeItTalk/facewarp/facewarp/bin/facewarp'
        ### via container
        warp_linux = "/workspace/avatar/facewarp_linux/facewarp/bin/facewarp"

        if os.path.exists(os.path.join(output_dir, "output")):
            shutil.rmtree(os.path.join(output_dir, "output"))
        os.mkdir(os.path.join(output_dir, "output"))
        initial_dir = os.getcwd()
        os.chdir("{}".format(os.path.join(output_dir, "output")))
        cur_dir = os.getcwd()
        print(f"Current directory is {cur_dir}")
        print(f"The listdir in {output_folder} is {os.listdir(output_folder)}")
        tmp = "pred_fls_out_audio_embed"
        tmp_folder = os.path.join(output_folder, tmp)
        print(f"The listdir in {tmp_folder} is {os.listdir(tmp_folder)}")
        tmp_folder = os.path.join(tmp_folder, 'output')
        print(f"The listdir in {tmp_folder} is {os.listdir(tmp_folder)}")
        
        os.system(
            f"{warp_linux} "
            f"{os.path.join('../../../../av_speech_synthesis/examples_cartoon', parser.jpg)} "
            f"{os.path.join(cur_dir, '..', 'triangulation.txt')} "
            f"{os.path.join(cur_dir, '..', 'reference_points.txt')} "
            f"{os.path.join(cur_dir, '..', 'warped_points.txt')} "
            f"{os.path.join('../../../../av_speech_synthesis/examples_cartoon', parser.jpg_bg)} "
            "-novsync -dump"
        )
        # os.system(f'{warp_linux} '
        # f"{os.path.join('../../../examples_cartoon', opt_parser.jpg)} "
        # f"{os.path.join(cur_dir, '..', 'triangulation.txt')} "
        # f"{os.path.join(cur_dir, '..', 'reference_points.txt')} "
        # f"{os.path.join(cur_dir, '..', 'warped_points.txt')} "
        # f"{os.path.join('../../../examples_cartoon', opt_parser.jpg_bg)} "
        # "-novsync -dump"
        # )
        # '-novsync -dump'))
        print("AFTER CALLING FACEWARP")
        cur_dir = os.getcwd()
        print(f"Current directory is {cur_dir}")
        print(f"The listdir in {output_folder} is {os.listdir(output_folder)}")
        tmp = "pred_fls_out_audio_embed"
        tmp_folder = os.path.join(output_folder, tmp)
        print(f"The listdir in {tmp_folder} is {os.listdir(tmp_folder)}")
        tmp_folder = os.path.join(tmp_folder, 'output')
        all_tgas = os.listdir(tmp_folder)
        all_tgas.sort()
        print(f"The listdir in {tmp_folder} is {all_tgas}")
        print(f"The input folder is {input_folder} and the ain is {ain}")

        print("Begin Rendering ----------------###")
        new_ain = ain.split(".wav")[0]
        os.system(
            'ffmpeg -y -r 62.5 -f image2 -i "%06d.tga" -i {} -pix_fmt yuv420p -vf "pad=ceil(iw/2)*2:ceil(ih/2)*2" -shortest {}'.format(
                os.path.join("../../..", input_folder, ain),
                os.path.join(cur_dir, "..", f"{new_ain}.mp4"),
            )
        )

        all_files = os.listdir()

        # for trash in all_files:
        #     if trash.endswith(".tga"):
        #         os.remove(trash)

        # os.chdir("{}".format(os.path.join(cur_dir, "../")))
        # os.removedirs("output")
        # os.remove("reference_points.txt")
        # os.remove("triangulation.txt")
        # os.remove("warped_points.txt")
        # # back to the root folder
        # cur_dir = os.getcwd()
        # print(f"Current directory is {cur_dir}")
        # os.chdir("{}".format(os.path.join(cur_dir, "../../")))
        # os.chdir("{}".format(os.path.join(cur_dir, '../../../')))

        print(f"Saved mp4 from Current directory {cur_dir} to ../cur_dir")
        shutil.copy(
            os.path.join(cur_dir, "..", f"{new_ain}.mp4"),
            "/workspace/avatar/init_out.mp4"
            )
        # return to initial dir
        os.chdir(initial_dir)
        return os.path.join(cur_dir, "..", f"{new_ain}.mp4")

    return generate_kripis


if __name__ == "__main__":
    """
    Launches text to speech (inference).
    Inference is executed on a single GPU or CPU.
    """
    parser = argparse.ArgumentParser(description="PyTorch Tacotron 2 Inference")
    parser = parse_args_kripis(parser)
    # args, _ = parser.parse_known_args()
    kripis_parser = parser.parse_args()

    # path to checkpoints
    i2i = "/workspace/avatar/ckpt_116_i2i_comb.pth"
    autovc = "/workspace/avatar/ckpt_autovc.pth"
    spk_branch = "/workspace/avatar/ckpt_speaker_branch.pth"
    content_branch = "/workspace/avatar/ckpt_content_branch.pth"

    # path to IO folders
    input_folder = "/workspace/avatar/av_requests/wav/A1"
    output_folder = "/workspace/avatar/av_requests/mp4/A1"
    # dump folder
    dump_folder = "/workspace/avatar/av_speech_synthesis/tmp/dump"
    # cpu arg
    cpu = True

    # overwrite args
    kripis_parser.dump_folder = dump_folder
    kripis_parser.load_AUTOVC_name = autovc
    kripis_parser.load_a2l_G_name = spk_branch
    kripis_parser.load_a2l_C_name = content_branch
    kripis_parser.load_G_name = i2i
    kripis_parser.output_folder = output_folder

    # get main wav--->mp4 script
    generate_kripis = make_kripis_fn(
        kripis_parser,
        i2i,
        autovc,
        spk_branch,
        content_branch,
        amp_lip_x=2.0,
        amp_lip_y=2.0,
        amp_pos=0.5,
        output_folder=output_folder,
        input_folder=input_folder,
        dump_folder=dump_folder,
        cpu=cpu,
    )

    generate_kripis("A1.wav")
